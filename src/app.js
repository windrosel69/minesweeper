import './style.css'

const startGame = (width, height, bombsCount) => {
    // отрисовка поля
    const mainBlock = document.querySelector('.mainBlock') // ищем блок с классом mainBlock
    const allPoints = width * height // кол-во ячеек
    mainBlock.innerHTML = '<button></button>'.repeat(allPoints) // заполняем наше allPoints кнопками (Button)
    const cells = [...mainBlock.children]
    // console.log(mainBlock)
    let closedCount = allPoints
//--------------------------------------------------------
    // реализация создания мин
    const bombs = [...Array(allPoints).keys()] // результатом будет массив с ключами от 0 до 63 и со значениями от 1 до 64 соответственно
        .sort(() => Math.random() - 0.5) // реализуем случайное располложение значений в массиве 
        .slice(0, bombsCount) // и вырезаем из массива от 0 до bombsCount значения, данный клетки и будут бомбами.
        // console.log(bombs)
        // console.log(Array.isArray(bombs))
//--------------------------------------------------------
    // реализация проверки нажатия на мину
    const isMine = (row, column) => { // создаём функцию с параметрами row и column, который отвечают за номер столбца и колонки соответственно
        if (!isValid(row, column)) return false
        const index = row * width + column // вычисляем индекс ячейки на которую нажали
        return bombs.includes(index) // и сравниваем её с теми индексами в которых находятся бомбы
    }
//--------------------------------------------------------
    // функция вывода результатов
    const open = (row, column) => {
        if (!isValid(row, column)) return // проверка на валидность.
        const index = row * width + column
        const cell = cells[index] // расскрываем данную ячейку
        // console.log (cell)
        if (cell.disabled === true) return //проверка на открытие ячейки.
        
        cell.disabled = true
        if(isMine(row, column)) { // проверка, если в ячейке мина, выводит X в ячейке, выводит сообщение о проигрыше, игра заканчивается.
            cell.innerHTML = 'X'
            // console.log(isMine(row, column))
            alert('Вы проиграли')
            location.reload()
            return
        }

        closedCount--
        if (closedCount <= bombsCount) { // проверка, на выйгрыш.
            alert('Вы выиграли')
            location.reload()
            return
        }
        const count = getCount(row, column) // вывод в ячейку информации о кол-ве мин в соседних ячейках
        if (count !== 0) {
            cell.innerHTML = count
            return
        }
        for (let x = -1; x <= 1; x++) { // реализация открытия прилигающих пустых ячеек
            for (let y = -1; y <= 1; y++) {
                open(row + y, column + x)
            }
        }
    }
//--------------------------------------------------------
    // проверка на валидность
    function isValid(row, column) {
        return row >= 0
            && row < height
            && column >= 0
            && column < width
    }
//--------------------------------------------------------
    // реализация подсчёта кол-ва мин вокруг
    const getCount = (row, column) => {
        let count = 0
        for (let x = -1; x <= 1; x++) { // с помощью циклов поочерёдно проходимся по всем соседним ячейкам и делаем проверку на наличие мин
            for (let y = -1; y <= 1; y++) {
                if (isMine(row + y, column + x)) { // если мина есть, увеличиваем count на +1, если нет count не меняем, цикл проверяет другу ячейку
                    count++
                }
            }
        }
        return count
    }
//--------------------------------------------------------
    // реализация события "клик"
    mainBlock.addEventListener('click', (event) => {
        if (event.target.tagName !== 'BUTTON') { // проверка на случайное нажатие вне рабочую область
            return
        }
        
        const index = cells.indexOf(event.target) // получаем индекс той кнопки на которую нажали
        // console.log('индекс', index)
        const column = index % width // высчитываем значение колонки для данной ячейки
        // console.log('колонка', column)
        const row = (index - column) / width // высчитываем значение ряда для данной ячейки
        // console.log('ряд', row)
        // console.log('----------------------------------')
        open(row, column) 
    })
    
}
startGame(15, 15, 30)